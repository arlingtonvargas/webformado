﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ET.ADTerceros;
using DA.ADTercerosDA;

namespace BL
{
    public class ADTercerosBL
    {
        public List<ADTercerosET> GetTerceros() 
        {
            ADTercerosDA ADTercerosDAcl = new ADTercerosDA(); 
            List<ADTercerosET> ListaTerceros = new List<ADTercerosET>();
            try
            {
                ListaTerceros = ADTercerosDAcl.GetTerceros();
            }
            catch (Exception ex)
            {
            }
            return ListaTerceros;
        }
    }
}
