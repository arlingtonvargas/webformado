﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.ADTerceros
{
    public class ADTercerosET
    {
        public long IdTercero { get; set; }
        public string Pnombre { get; set; }
        public string Snombre { get; set; }
        public string Papellido { get; set; }
        public string Sapellido { get; set; }
        public string Direccion { get; set; }
        public long Telefono { get; set; }
        public DateTime FechaCrea { get; set; }
        public DateTime FechaMod { get; set; }
        public int UsuCrea { get; set; }
        public int UsuMod { get; set; }
        public int IdUsuario { get; set; }
        public int Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string LugarNaciento { get; set; }
        public string FotoString64 { get; set; }
        public int TipoTercero { get; set; }
        public int TipoDoc { get; set; }
        public byte[] Foto { get; set; }
    }    
}
