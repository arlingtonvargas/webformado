﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WFdemo.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Lista de terceros</h1>
            <table class="table" id="tablaProductos">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">IdTercero</th>
                        <th scope="col">Pnombre</th>
                        <th scope="col">Snombre</th>
                        <th scope="col">Papellido</th>
                        <th scope="col">Sapellido</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">FechaCrea</th>
                        <th scope="col">FechaMod</th>
                        <th scope="col">UsuCrea</th>
                        <th scope="col">UsuMod</th>
                        <th scope="col">IdUsuario</th>
                        <th scope="col">Sexo</th>
                        <th scope="col">FechaNacimiento</th>
                        <th scope="col">LugarNaciento</th>
                        <th scope="col">TipoTercero</th>
                        <th scope="col">TipoDoc</th>
                        <th scope="col">Imagen</th>
                    </tr>
                </thead>
            </table>
        </div>
    </form>

    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: 'Default.aspx/GetTerceros',
                type: 'get',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);                    
                    for (var i = 0; i < data.d.length; i++) {
                        var row = '';
                        row = '<tr> ' +
                                    '<td>' + parseInt(i + 1) + '</td>' +
                                    '<td><span>' + data.d[i].IdTercero + '</span></td>' +
                                    '<td><span>' + data.d[i].Pnombre + '</span></td>' +
                                    '<td><span>' + data.d[i].Snombre + '</span></td>' +
                                    '<td><span>' + data.d[i].Papellido + '</span></td>' +
                                    '<td><span>' + data.d[i].Sapellido + '</span></td>' +
                                    '<td><span>' + data.d[i].Direccion + '</span></td>' +
                                    '<td><span>' + data.d[i].Telefono + '</span></td>' +
                                    '<td><span>' + parseJsonDate(data.d[i].FechaCrea)+ '</span></td>' +
                                    '<td><span>' + parseJsonDate(data.d[i].FechaMod) + '</span></td>' +
                                    '<td><span>' + data.d[i].UsuCrea + '</span></td>' +
                                    '<td><span>' + data.d[i].UsuMod + '</span></td>' +
                                    '<td><span>' + data.d[i].IdUsuario + '</span></td>' +
                                    '<td><span>' + data.d[i].Sexo + '</span></td>' +
                                    '<td><span>' + parseJsonDate(data.d[i].FechaNacimiento) + '</span></td>' +
                                    '<td><span>' + data.d[i].LugarNaciento + '</span></td>' +
                                    '<td><span>' + data.d[i].TipoTercero + '</span></td>' +
                                    '<td><span>' + data.d[i].TipoDoc + '</span></td>';
                        if (data.d[i].Foto != null) {
                            row += '<td><img height="42" src= "data:image/jpg;base64,' + data.d[i].FotoString64 + '" /></td>' +
                                 '</tr>'
                        } else {
                            row += '<td><span>Sin imagen</span></td>' +
                                 '</tr>'
                        }                      
                        $('#tablaProductos').append(row);
                    }
                },
                error: function (err) {
                    console.log(err);
                }
            });
        });

        function parseJsonDate(jsonDate) {

            var fullDate = new Date(parseInt(jsonDate.substr(6)));
            debugger;
            var twoDigitMonth = (fullDate.getMonth() + 1) + ""; if (twoDigitMonth.length == 1) twoDigitMonth = "0" + twoDigitMonth;

            var twoDigitDate = fullDate.getDate() + ""; if (twoDigitDate.length == 1) twoDigitDate = "0" + twoDigitDate;
            var currentDate = twoDigitMonth + "/" + twoDigitDate + "/" + fullDate.getFullYear() +" "+ fullDate.getHours()+ " : "+ fullDate.getMinutes();

            return currentDate;
        };

    </script>
</body>
</html>
