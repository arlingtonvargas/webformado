﻿using BL;
using ET.ADTerceros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WFdemo
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static List<ADTercerosET> GetTerceros()
        {

            ADTercerosBL bl = new ADTercerosBL();


            var bl1 = bl.GetTerceros();

            bl1.ForEach(t => {

                if (t.Foto != null)
                {
                    t.FotoString64 = Convert.ToBase64String(t.Foto);
                   var gola =  t.FotoString64.Length;
                }
                else 
                { 
                    t.FotoString64 = "";
                }
               
            });
            
            return bl1;
        }
    }
}