﻿using ET.ADTerceros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.ADTercerosDA
{
    public class ADTercerosDA
    {
        public List<ADTercerosET> GetTerceros()
        {
            List<ADTercerosET> ListaTerceros = new List<ADTercerosET>();
            try
            {
                string sql = "SELECT IdTercero, Pnombre, Snombre, Papellido, Sapellido, Direccion, Telefono, " +
                " FechaCrea, FechaMod, UsuCrea, UsuMod, IdUsuario, Sexo, FechaNacimiento," +
                " LugarNaciento, TipoTercero, TipoDoc, Foto, FotoString64 FROM ADTerceros";
                ListaTerceros = Connection.ClFunciones.DataReaderMapToList<ADTercerosET>(sql);
            }
            catch (Exception ex)
            {
            }
            return ListaTerceros;
        }
    }
}
