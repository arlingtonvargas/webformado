﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DA.Connection
{
    public class ClFunciones
    {
        public static List<T> DataReaderMapToList<T>(string sql)
        {
            List<T> list = new List<T>();
            try
            {
                string cad = Connection.CadenaConexion;
                using (SqlConnection conn = new SqlConnection(cad))
                {
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    conn.Open();
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {
                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    prop.SetValue(obj, dr[prop.Name], null);
                                }
                            }
                            list.Add(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
            }            
            return list;
        }
    }
}
